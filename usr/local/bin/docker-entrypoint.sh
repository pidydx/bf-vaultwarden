#!/bin/bash

set -e

if [ "$1" = 'init' ]; then
    echo -n ${ADMIN_TOKEN} | argon2 "$(openssl rand -base64 32)" -e -id -k 19456 -t 2 -p 1 > /var/lib/vaultwarden/admin_token_hash
    exec echo "Admin token hash created."
fi

if [ "$1" = 'vaultwarden' ]; then
    source /etc/vaultwarden.env
    export ADMIN_TOKEN=`cat /var/lib/vaultwarden/admin_token_hash`
    exec vaultwarden
fi

exec "$@"