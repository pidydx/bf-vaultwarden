#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=vaultwarden
ENV APP_GROUP=vaultwarden

ENV VAULTWARDEN_VERSION=1.32.0
ENV BITWARDEN_WEB_VERSION=2024.6.2

ENV BASE_PKGS argon2 libpq5 libmariadb-dev-compat

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -m -N -u 1000 ${APP_USER} -g ${APP_GROUP} -d /var/lib/${APP_USER} -s /usr/sbin/nologin

# Update and install base packages
COPY etc/apt /etc/apt

RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*

##########################
# Create build container #
##########################
FROM base AS builder

ENV NODE_OPTIONS="--max_old_space_size=4096"

# Set build dependencies
ENV BUILD_DEPS build-essential cargo-1.80 curl git gnupg libmariadb-dev libpq-dev nodejs pkg-config wget

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS} \
 && ln -s /usr/bin/cargo-1.80 /usr/bin/cargo

# Run build
WORKDIR /usr/src
RUN wget -nv https://github.com/dani-garcia/vaultwarden/archive/refs/tags/${VAULTWARDEN_VERSION}.tar.gz \
 && tar -zxf ${VAULTWARDEN_VERSION}.tar.gz \
 && wget -nv https://github.com/dani-garcia/bw_web_builds/archive/refs/tags/v${BITWARDEN_WEB_VERSION}.tar.gz \
 && tar -zxf v${BITWARDEN_WEB_VERSION}.tar.gz \
 && wget -nv https://github.com/bitwarden/clients/archive/refs/tags/web-v${BITWARDEN_WEB_VERSION}.tar.gz \
 && tar -zxf web-v${BITWARDEN_WEB_VERSION}.tar.gz

WORKDIR /usr/src/vaultwarden-${VAULTWARDEN_VERSION}
RUN cargo build --features sqlite,mysql --release \
 && mv target/release/vaultwarden /usr/local/bin/vaultwarden \
 && cargo-1.80 clean

WORKDIR /usr/src/clients-web-v${BITWARDEN_WEB_VERSION}
RUN cp -vfR ../bw_web_builds-${BITWARDEN_WEB_VERSION}/resources/src/* ./apps/web/src/ \
 && vw_admin_logo=`tr -d '\n' < ../bw_web_builds-${BITWARDEN_WEB_VERSION}/resources/vaultwarden-admin-console-logo.svg` \
 && sed -i "s|\<svg.*\/svg\>|$vw_admin_logo|" ./apps/web/src/app/admin-console/icons/admin-console-logo.ts \
 && vw_pw_logo=`tr -d '\n' < ../bw_web_builds-${BITWARDEN_WEB_VERSION}/resources/vaultwarden-password-manager-logo.svg` \
 && sed -i "s|\<svg.*\/svg\>|$vw_pw_logo|" ./apps/web/src/app/layouts/password-manager-logo.ts \
 && rm -rf ./bitwarden_license/ \
 && patch -p1 < ../bw_web_builds-${BITWARDEN_WEB_VERSION}/patches/v${BITWARDEN_WEB_VERSION}.patch \
 && npm ci \
 && npm audit fix || true \
 && npx update-browserslist-db@latest \
 && cd ./apps/web \
 && npm run dist:oss:selfhost \
 && printf '{"version":"%s"}' ${BITWARDEN_WEB_VERSION} > build/vw-version.json \
 && find build -name "*.map" -delete \
 && mkdir -p /usr/local/vaultwarden \
 && mv build /usr/local/vaultwarden/web-vault \
 && rm -rf ../../node_modules/


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

ENV DATA_FOLDER=/var/lib/vaultwarden
ENV WEB_VAULT_FOLDER=/usr/local/vaultwarden/web-vault/
ENV WEB_VAULT_ENABLED=true
ENV EXTENDED_LOGGING=true
ENV USE_SYSLOG=false
ENV ROCKET_PROFILE="release"
ENV ROCKET_ADDRESS=0.0.0.0
ENV ROCKET_PORT=8000
ENV WEBSOCKET_ENABLED=
ENV ORG_EVENTS_ENABLED=true
ENV EVENTS_DAYS_RETAIN=365
ENV SIGNUPS_ALLOWED=false
ENV INVITATIONS_ALLOWED=true
ENV SIGNUPS_VERIFY=true

RUN mkdir -p /var/lib/vaultwarden \
 && chown ${APP_USER}:${APP_GROUP} /var/lib/vaultwarden

EXPOSE 8000/tcp
VOLUME ["/var/lib/vaultwarden"]

USER $APP_USER
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["vaultwarden"]
